package com.mycompany.blelib_java;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.bluetooth.le.ScanFilter.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.provider.Settings;
import android.util.Log;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amazonaws.util.IOUtils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;


public class AndroidBLE {

    private Activity context;
    private BluetoothManager bluetoothManager;
    public BluetoothLeScanner bleScanner = null;
    private BluetoothGatt bluetoothGatt;
    private BluetoothAdapter bluetoothAdapter;
    public List<BluetoothDevice> scanResults;
    public int n_devices;
    public boolean message_recieved =false;
    public List<BluetoothDevice> Bdevices;

    private boolean isScanning=false;
    private final UUID serviceUuid = UUID.fromString("000000ff-0000-1000-8000-00805f9b34fb");
    private final UUID serviceCharUuid = UUID.fromString("0000ff01-0000-1000-8000-00805f9b34fb");
    private final UUID cccDescriptorUUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private final UUID controlCharUUID = UUID.fromString("0000ff03-0000-1000-8000-00805f9b34fb");
    private ScanFilter scanFilter;
    private ScanSettings scanSettings;
    private ScanCallback scanCallback;
    private BluetoothGattCallback gattCallback;

    private static final String LOGTAG = "AG";

    //for debugging purposes, to check if a device was found
    public boolean deviceFound = false;
    public String ourDeviceName;


    //check if there was a notify message
    public boolean notified = false;
    public String testSuccess = "No results";


    //min and max values for our motor
    private final float min = 60f;
    private final float max = 100f;

    public AndroidBLE() {
    }

    //function for initializing everything using the context from "currentActivity" from "com.unity3d.player.UnityPlayer"
    public final void setContext( Activity context) {
        Bdevices= new ArrayList<BluetoothDevice>();

        System.out.println("Main Thread ID: " + Thread.currentThread().getId());
        System.out.println("Setting context");
        this.context = context;
        this.bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = bluetoothManager.getAdapter();

        this.scanResults = new ArrayList<BluetoothDevice>();

        //all the necessities for BLE scan are also initialised here:

        //this is the scan filter  - only the bluetooth devices with our UUID will be found in the scan
        this.scanFilter = (new Builder()).setServiceUuid(ParcelUuid.fromString(this.serviceUuid.toString())).build();
        this.scanSettings = (new android.bluetooth.le.ScanSettings.Builder()).setScanMode(2).build();
        this.scanCallback = new ScanCallback() {
            public void onScanResult(int callbackType, @NotNull ScanResult result) {
                super.onScanResult(callbackType, result);
                BluetoothDevice device = result.getDevice();
                boolean new_device = false;
                for (int i = 0; i<scanResults.size(); i++){
                    if(!scanResults.get(i).getAddress().equals(device.getAddress())){
                       new_device=true;
                    }
                }
                if(new_device||scanResults.size()==0) {
                    Log.i("AG", "New scan result");
                    //right now the first found device is connected to automatically - nothing will be displayed in Unity

                    Log.i("AG", device.getName());
                    AndroidBLE.this.scanResults.add(device);
                    AndroidBLE.this.n_devices = scanResults.size();
                    AndroidBLE.this.ourDeviceName = device.getName();
                    //   deviceFound = true;
                    //connect(device);
                }
            }

            public void onScanFailed(int errorCode) {
                Log.e("ScanCallback", "onScanFailed: code " + errorCode);
            }
        };

        //Callback funtions for all BLE functionalities (Read, Write, Connect, Disconnect etc.) are defined here
        this.gattCallback = new BluetoothGattCallback() {
            public void onConnectionStateChange(@NotNull BluetoothGatt gatt, int status, int newState) {
                BluetoothDevice device = gatt.getDevice();
                String deviceAddress = device.getAddress();
                if (status == 0) {
                    if (newState == 2) {
                        Log.i("AG", "Successfully connected to " + deviceAddress);
                        AndroidBLE.this.bluetoothGatt = gatt;
                        (new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
                            public final void run() {
                                BluetoothGatt gatt = AndroidBLE.this.bluetoothGatt;
                                gatt.discoverServices();
                            }
                        }));

                    } else if (newState == 0) {
                        Log.w("BluetoothGattCallback", "Successfully disconnected from " + deviceAddress);
                        gatt.close();
                    }
                } else {
                    Log.w("BluetoothGattCallback", "Error " + status + " encountered for " + deviceAddress + "! Disconnecting...");
                    gatt.close();
                }

            }

            public void onServicesDiscovered(@NotNull BluetoothGatt gatt, int status) {
                StringBuilder var10001 = (new StringBuilder()).append("Discovered ").append(gatt.getServices().size()).append(" services for ");
                BluetoothDevice mDevice = gatt.getDevice();
                Log.w("BluetoothGattCallback", var10001.append(mDevice.getAddress()).toString());
            }


            public void onCharacteristicWrite(@NotNull BluetoothGatt gatt, @NotNull BluetoothGattCharacteristic characteristic, int status) {
                boolean var4 = false;
                boolean var5 = false;
                switch(status) {
                    case 0:
                        StringBuilder var10001 = (new StringBuilder()).append("Wrote to characteristic ").append(characteristic.getUuid()).append(" | value: ");
                        AndroidBLE var10002 = AndroidBLE.this;
                        byte[] var10003 = characteristic.getValue();
                        Log.i("BluetoothGattCallback", var10001.append(var10003.toString()).toString());
                        break;
                    case 3:
                        Log.e("BluetoothGattCallback", "Write not permitted for " + characteristic.getUuid() + '!');
                        break;
                    case 13:
                        Log.e("BluetoothGattCallback", "Write exceeded connection ATT MTU!");
                        break;
                    default:
                        Log.e("BluetoothGattCallback", "Characteristic write failed for " + characteristic.getUuid() + ", error: " + status);
                }

            }


            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onCharacteristicRead(gatt, characteristic, status);
            switch (status){
                case BluetoothGatt.GATT_SUCCESS:
                    Log.i("BluetoothGattCallback", "Read characteristic $uuid:\n${value.toHexString()}");

            }

            }

            @Override
            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                super.onDescriptorWrite(gatt, descriptor, status);

                Log.i("AG", "onDescriptorWrite :" + ((status == BluetoothGatt.GATT_SUCCESS) ? "Sucess" : "false"));


            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicChanged(gatt, characteristic);

                Log.i("AG", "Characteristic $uuid changed:\n${value.toHexString()}");

                byte [] bytes = characteristic.getValue();

                testSuccess = "\n" + byteArrayToHex(bytes);

                Log.i("AG","Notified: " + testSuccess);
                notified = true;
                message_recieved=true;
            }
        };
        Log.i("AG", "created Plugin");
    }


    public void checkConnected()
    {
        // true == headset connected && connected headset is support hands free
        int state = BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(BluetoothProfile.HEADSET);
        if (state != BluetoothProfile.STATE_CONNECTED)
            return;

        try
        {
            BluetoothAdapter.getDefaultAdapter().getProfileProxy(this.context, serviceListener, BluetoothProfile.HEADSET);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private BluetoothProfile.ServiceListener serviceListener = new BluetoothProfile.ServiceListener()
    {
        private final int[] states={ BluetoothProfile.STATE_DISCONNECTING,
                BluetoothProfile.STATE_DISCONNECTED,
                BluetoothProfile.STATE_CONNECTED,
                BluetoothProfile.STATE_CONNECTING};
        @Override
        public void onServiceDisconnected(int profile)
        {

        }

        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy)
        {
            Bdevices.addAll(proxy.getDevicesMatchingConnectionStates(states));
            for (BluetoothDevice device : Bdevices)
            {

                Log.i("AG", "|" + device.getName() + " | " + device.getAddress() + " | " + proxy.getConnectionState(device) + "(connected = "
                        + BluetoothProfile.STATE_CONNECTED + ")");
            }

        }
    };

    public List<BluetoothDevice> getConnectedDevices(){
        return bluetoothManager.getConnectedDevices(BluetoothProfile.GATT) ;
    }
    public List<BluetoothDevice> getConnectedDevices2(){
        return bluetoothManager.getConnectedDevices(BluetoothProfile.GATT_SERVER) ;
    }
    public String returnConnectedDevices(){

     //   this.bluetoothAdapter.getProfileProxy(this.context, serviceListener, BluetoothProfile.HID_DEVICE);
        this.bluetoothAdapter.getProfileProxy(this.context, serviceListener, BluetoothProfile.GATT_SERVER);
        this.bluetoothAdapter.getProfileProxy(this.context, serviceListener, BluetoothProfile.GATT);
      //  this.bluetoothAdapter.getProfileProxy(this.context, serviceListener, BluetoothProfile.HEADSET);

        String list = "";
        for (BluetoothDevice i:getConnectedDevices()) {
            list +=i.getName() + "\n";

        }
       
        for (BluetoothDevice i: this.Bdevices) {
            list +=i.getName() + "\n";

        }

        return list;
    }



    public BluetoothDevice giveDevice(int n){
        return scanResults.get(n);
    }

    private void connect_n(int n){
        connect(giveDevice(n));
        deviceFound = true;
    }
    private void connect(BluetoothDevice device) {
        AndroidBLE.this.bleScanner.stopScan(scanCallback);
        isScanning=false;
        device.connectGatt((Context)AndroidBLE.this.context, false, AndroidBLE.this.gattCallback);
    }
    private void disconnect(){
        this.bluetoothGatt.disconnect();
    }


    public final void startBleScan() {
        if (!this.isScanning) {
            this.bleScanner = this.bluetoothAdapter.getBluetoothLeScanner();
            Log.i("AG " , this.bluetoothAdapter.getAddress());
            this.bleScanner.startScan( Collections.singletonList(scanFilter), scanSettings, this.scanCallback);
            this.isScanning = true;
        }
        else{
            System.out.print("scanning is already on");
        }

    }

    private final void stopBleScan() {
        if (this.isScanning) {
            this.bleScanner.stopScan(this.scanCallback);
            this.isScanning = false;
        }

    }





    public void writeFromString(String uuid, byte[] payload) throws  Throwable{
        Log.i("AG", "writing to "+uuid);
        writeCharacteristic(this.bluetoothGatt.getService(serviceUuid).getCharacteristic(UUID.fromString(uuid)), payload);
    }

    private void writeCharacteristic(BluetoothGattCharacteristic characteristic, byte[] payload) throws Throwable {
        int writeType;
        if (this.isWritable(characteristic)) {
            writeType = 2;
        } else {
            if (!this.isWritableWithoutResponse(characteristic)) {
                throw (Throwable)(new IllegalStateException("Characteristic " + characteristic.getUuid() + " cannot be written to"));
            }

            writeType= 1;
        }

        characteristic.setWriteType(writeType);
        characteristic.setValue(payload);
        this.bluetoothGatt.writeCharacteristic(characteristic);
    }



    public final boolean isWritable(@NotNull BluetoothGattCharacteristic $this$isWritable) {
        return this.containsProperty($this$isWritable, 8);
    }

    public final boolean isWritableWithoutResponse(@NotNull BluetoothGattCharacteristic $this$isWritableWithoutResponse) {
        return this.containsProperty($this$isWritableWithoutResponse, 4);
    }

    public final boolean containsProperty(@NotNull BluetoothGattCharacteristic $this$containsProperty, int property) {
        return ($this$containsProperty.getProperties() & property) != 0;
    }

    public final boolean isReadable(@NotNull BluetoothGattCharacteristic $this$isReadable) {
        return this.containsProperty($this$isReadable, 2);
    }



    //NOTIFY FUNCTIONALITIES
    public final boolean isNotifiable(@NotNull BluetoothGattCharacteristic $this$isNotifiable) {
        return this.containsProperty($this$isNotifiable,16);
    }

    private void writeDescriptor(BluetoothGattDescriptor descriptor, byte[] payload) {
      descriptor.setValue(payload);
      this.bluetoothGatt.writeDescriptor(descriptor);
      Log.i("AG", "writeDexriptor "+descriptor.getUuid().toString());
    }

    public void enableNotifications(/*BluetoothGattCharacteristic bluetoothGattCharacteristic*/){
        //bluetoothGattCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
        BluetoothGattCharacteristic bluetoothGattCharacteristic = this.bluetoothGatt.getService(serviceUuid).getCharacteristic(controlCharUUID);
        byte[] payload = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;;
/*        if(isNotifiable(bluetoothGattCharacteristic)){
            payload=BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
        }else {
            Log.i("AG: ConnectionManager","Characteristic doesn't support NOTIFY");
            return;
        }*/


        this.bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic,true);
        BluetoothGattDescriptor cccDescriptor = bluetoothGattCharacteristic.getDescriptor(cccDescriptorUUID);
        writeDescriptor(cccDescriptor,payload);
       /* if(this.bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic,true)){
            writeDescriptor(cccDescriptor,payload);
        } else{
            Log.i("AG: ConnectionManager","Characteristic failed for setCharacteristicNotification");
            return;
        }*/
    }


    public void disableNotification(BluetoothGattCharacteristic bluetoothGattCharacteristic){

        if(!isNotifiable(bluetoothGattCharacteristic)){
            Log.i("AG: ConnectionManager","Characteristic doesn't support NOTIFY");
            return;
        }

        BluetoothGattDescriptor cccDescriptor = bluetoothGattCharacteristic.getDescriptor(cccDescriptorUUID);
        if(this.bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic,false)){
            writeDescriptor(cccDescriptor,BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        } else{
            Log.i("AG: ConnectionManager","Characteristic failed for setCharacteristicNotification");
            return;
        }
    }

    public void checkGPSenabled() {
        //check if GPS is enabled
        LocationManager l_manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!l_manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this.context);
            alertDialog.setTitle("GPS setting!");
            alertDialog.setMessage("This application requires Location Services. Do you want to enable it?");
            alertDialog.setPositiveButton("Go to Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }
    private void readCharacteristic(BluetoothGattCharacteristic characteristic)  {

        if(this.isReadable(characteristic)){
            this.bluetoothGatt.readCharacteristic(characteristic);
        }

    }



    private void controlCar(float x, float y) throws Throwable {

        BluetoothGattCharacteristic characteristic = this.bluetoothGatt.getService(serviceUuid).getCharacteristic(serviceCharUuid);
        byte[] payload;
        float tmp_x=betrag(x);
        float tmp_y = betrag(y);


        //adding a buffer zone: accept values over 0.25/-0.25
        float buffer_x=0.2f;
        float buffer_y=0.25f;

        if(tmp_y<buffer_y ) {tmp_y = 0.0f; }
        if(tmp_x<buffer_x) tmp_x=0.0f;

        //scaling the values between 0.2 - 1 to our range
        int p_x = (int) (((tmp_x-buffer_x) * (max - min) / (1.0f-buffer_x)) + min);
        int p_y = (int) (((tmp_y-buffer_y) * (max - min) / (1.0f-buffer_y)) + min);

        Log.i("AG", "Acceleration "+String.valueOf(p_y)+" Rotation "+String.valueOf(p_x));

        byte pow_x = hexToByte(Integer.toHexString(p_x));
        byte pow_y = hexToByte(Integer.toHexString(p_y));
        if (x == 0f) {
            if (y > 0f) {
                //FRONT
                payload = new byte[]{(byte) 0x01, pow_y, (byte) 0x00};
            } else {
                //BACK
                payload = new byte[]{(byte) 0x02, pow_y, (byte) 0x00};
            }
        } else {
            if (y == 0) {
                if (x > 0f) {
                    //LEFT
                    payload = new byte[]{(byte) 0x03, (byte) 0x00, pow_x};
                } else {
                    //RIGHT
                    payload = new byte[]{(byte) 0x04, (byte) 0x00, pow_x};
                }
            } else if (y > 0f) {
                if (x < 0f) {
                    //FRONT LEFT
                    payload = new byte[]{(byte) 0x05, (byte) pow_y, pow_x};
                } else {
                    //FRONT RIGHT
                    payload = new byte[]{(byte) 0x06, (byte) pow_y, pow_x};
                }

            } else {
                if (x < 0f) {
                    //BACK LEFT
                    payload = new byte[]{(byte) 0x07, (byte) pow_y, pow_x};
                } else {
                    //BACK RIGHT
                    payload = new byte[]{(byte) 0x08, (byte) pow_y, pow_x};
                }

            }

        }
        writeCharacteristic(characteristic, payload);

    }
    private void stopCar(){
        //stop command
        byte[] payload = new byte[] {(byte) 0x00, (byte)0x00, (byte)0x00};
        BluetoothGattCharacteristic characteristic = this.bluetoothGatt.getService(serviceUuid).getCharacteristic(serviceCharUuid);
        characteristic.setWriteType(2);
        characteristic.setValue(payload);
        this.bluetoothGatt.writeCharacteristic(characteristic);
    }

    private float betrag(float i){
        if(i<0.0){
            return i*-1f;
        }
        else return i;
    }

    private Byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        if(hexString.length()>1){ int secondDigit = toDigit(hexString.charAt(1));
            return Integer.valueOf(((firstDigit << 4) + secondDigit)).byteValue();}
        else return Integer.valueOf(firstDigit).byteValue();

    }

    private int toDigit( char hexChar) {
        int digit = Character.digit(hexChar, 16);
        return digit;
    }

    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a) {
            sb.append(String.format("%02x", (b & 0x7F)));
            sb.append(' ');
        }
        return sb.toString().trim();
    }




}
